<?php
  include(".\index.html");
  
    function Produto($x, $y){   
      if(VeriProduto($x,$y)){
        return $x * $y;
      }

      return " <br>Axioma invalido !";   
    }

    function Soma($x, $y){
      if(VeriSoma($x,$y)){
        return $x + $y;
      }

      return " <br>Axioma invalido !";    
    }

    function VeriProduto($x,$y){
        $z = 5;
        $epsilon = 0.0001;
        if(((($x * $y) * $z) - ($x * ($y * $z)) < $epsilon) &&         //Associatividade
           (($x * $y) == ($y * $x)) &&                                 //Comutatividade
           (($x * 1 == $x) && (1 * $y == $y)) &&                       //Elemento neutro
           (($x * pow($x,-1) == 1) && ($y * pow($y,-1) == 1) ||        //Inverso multiplicativo
           $x == 0 || $y == 0)){        
              
            return true;
          }       
        return false;
    }

    function VeriSoma($x,$y){
      $z = 5;
      $epsilon = 0.00001;
        if(((($x + $y) + $z) - ($x + ($y + $z)) < $epsilon) &&  //Associatividade
           (($x + $y) == ($y + $x)) &&                          //Comutatividade
           (($x + 0 == $x) && ($y + 0 == $y)) &&                //Elemento neutro
           (($x + (-$x) == 0) && ($y + (-$y) == 0))){           //Simetrico
            
            return true;
        
           }
        return false;
    }

    $x = $_GET["x"];
    $y = $_GET["y"];
    
    if (is_numeric($x) && is_numeric($y)){
      $p = Produto($x,$y);
      $s = Soma($x,$y);
    }
    else{
      echo "<br><strong>Digite um valor valido!";
      return;
    }
    
    
    echo "<br><strong> Soma: $x + $y = $s<br>";
    echo "<br><strong> Produto: $x * $y = $p";
  
?>